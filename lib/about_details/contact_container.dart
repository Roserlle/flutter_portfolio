import 'package:flutter/material.dart';

import '/utils/themes.dart';

class ContactContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(20, 5, 20, 150),
            decoration: boxDecoration,
            child: ExpansionTile(
                leading: Icon(Icons.contact_phone, color: Colors.black),
                title: Text(
                    'Contact Information',
                    style: expansionStyle
                ),
                children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(
                            children: [
                                ListTile(
                                    leading: Icon(Icons.contact_support),
                                    title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'Linkedin:', 
                                                style: contactTitle
                                            ),
                                            Text(
                                                'https://www.linkedin.com/in/roselle-burlasa-b498a719a/', 
                                                style: textUnderline
                                            )
                                        ] 
                                    )
                                ),
                                ListTile(
                                    leading: Icon(Icons.laptop),
                                    title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'Portfolio:', 
                                                style: contactTitle
                                            ),
                                            Text(
                                                'rosellinda.github.io/rosellejburlasa_portfolio/', 
                                                style: textUnderline
                                            )
                                        ],
                                    )
                                )
                            ],
                        )
                    )   
                ],
            )
        );
    }
}