import 'package:flutter/material.dart';

import '/utils/class_function.dart';
import '/utils/themes.dart';

class ProfileContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Stack(
            children: [  
                Padding(  
                    padding: EdgeInsets.only(bottom: 30.0),  
                    child: ClipPath(  
                        clipper: ClippingClass(),  
                        child: Container(  
                            height: 130.0,  
                            decoration: BoxDecoration(color: drawerColor),  
                        ),  
                    ),  
                ),  
                Positioned.fill(  
                    child: Align(  
                        alignment: Alignment.bottomCenter,  
                        child: Container(  
                            height: 90.0,  
                            width: 90.0,  
                            decoration: BoxDecoration(  
                                image: DecorationImage(image: AssetImage('assets/profileImg.jpg')),  
                                shape: BoxShape.circle,  
                                border: Border.all(color: colorWhite, width: 5.0),  
                            ),  
                        )  
                    ),  
                )  
            ]
        );
    }
}