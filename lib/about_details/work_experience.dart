import 'package:flutter/material.dart';

import '/utils/class_function.dart';
import '/utils/themes.dart';

class WorkExperienceContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(20, 5, 20, 7),
            decoration: boxDecoration,
            child: ExpansionTile(
                leading: Icon(Icons.work, color: Colors.black),
                title: Text(
                    'Work Experience',
                    style: expansionStyle
                ),
                children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
                        child: Column(
                            children: [
                                ListTile(
                                    leading: MyBullet(),
                                    title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text('Flutter Developer Trainee', style: TextStyle(fontWeight: FontWeight.bold)),
                                            Text('FFUF Manila Inc'),
                                            Text('July 2021 - Present'),
                                        ],
                                    )
                                ),
                                ListTile(
                                    leading: MyBullet(),
                                    title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text('Transaction Processing Associate', style: TextStyle(fontWeight: FontWeight.bold)),
                                            Text('Accenture Inc'),
                                            Text('Feb 2016 - Aug 2020'),
                                        ]
                                    )
                                )
                            ],
                        )
                    )
                ],
            )
        );
    }
}