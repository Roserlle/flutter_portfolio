import 'package:flutter/material.dart';

import '/utils/list_function.dart';
import '/utils/themes.dart';

class InterestContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(20, 5, 20, 7),
            decoration: boxDecoration,
            child: ExpansionTile(
                leading: Icon(Icons.wallpaper, color: Colors.black),
                title: Text(
                    'Interests',
                    style: expansionStyle
                ),
                children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 7),
                        child: Column(
                            children: generateInterestList()
                        )
                    )
                ],
            )
        );
    }
}