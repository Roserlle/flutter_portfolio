import 'package:flutter/material.dart';

import '/utils/themes.dart';

class IntroductionContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(20, 35, 20, 7),
            decoration: boxDecoration,
            child: ExpansionTile(
                leading: Icon(Icons.face, color: Color.fromRGBO(20, 45, 68, 1)),
                title: Text('Introduction', style: expansionStyle),
                children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(25, 10, 25, 15),
                        child: Text(
                            'I am a Career-Shifter and a Coding Bootcamp Graduate. I discovered my passion for web development quite by accident. The experience of teaching myself about this field has been both challenging and extremely rewarding. I wake up excited about continuing to grow in this rapidly-evolving industry.',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight:FontWeight.w400
                            ),
                            textAlign: TextAlign.justify
                        )
                    )
                ],
            )
        );
    }
}