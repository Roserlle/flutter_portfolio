import 'package:flutter/material.dart';

import '../about_details/contact_container.dart';
import '../about_details/interest_container.dart';
import '../about_details/introduction.dart';
import '../about_details/name_container.dart';
import '../about_details/profile_container.dart';
import '../about_details/rive_animations.dart';
import '../about_details/skills_container.dart';
import '../about_details/work_experience.dart';

import '/utils/themes.dart';

void main() {
    runApp(App());
}

class App extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
        theme: themeData,
        home: MyHomePage(),
        );
    }
}

class MyHomePage extends StatefulWidget {
    @override
    _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
    Widget build(BuildContext context) {
        return Scaffold(   
            appBar: AppBar(
                backgroundColor: Color.fromRGBO(20, 45, 68, 1),
            ),
            resizeToAvoidBottomInset: false,
            body: SingleChildScrollView(
                child: Column(
                    children: [
                        Container(
                            height: (MediaQuery.of(context).size.height),
                            width: (MediaQuery.of(context).size.width),
                            color: drawerColor,
                            child: RiveAnimations()
                        ),
                        Container(
                            child: Column(
                                children: [
                                    ProfileContainer(),
                                    NameContainer(),
                                    IntroductionContainer(),
                                    WorkExperienceContainer(),
                                    SkillsContainer(),
                                    InterestContainer(),
                                    ContactContainer(),
                                    Container(
                                        margin: EdgeInsets.only(bottom: 80),
                                        child: Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Image.asset('assets/ffuf-logo.png', width: 100)
                                        )
                                    )
                                ] 
                            ),
                        )
                    ],  
                ),
            ),
        );
    }
}
