import 'package:flutter/material.dart';

import '/utils/class_function.dart';
import '/utils/themes.dart';

var skills = [
    'Flutter',
    'Dart',
    'Programming and Software Development',
    'Mobile Application Development',
    'Git',
    'Basic Japanese Language - N5 Passer'
];

var interests = [
    'Gaming',
    'Listening to Music',
    'Travelling',
    'Programming',
];

List<Widget> generateSkillList() {
    List<Widget> title = [];
    skills.forEach((skill) {
        title.add(
            ListTile(
                leading: MyBullet(),
                title: Text(skill, style: listTileStyle),
            )
        );
    });
    return title;
}

List<Widget> generateInterestList() {
    List<Widget> l = [];
    interests.forEach((interest) {
        l.add(
            ListTile(
                leading: MyBullet(),
                title: Text(interest, style: listTileStyle),
            )
        );
    });
    return l;
}